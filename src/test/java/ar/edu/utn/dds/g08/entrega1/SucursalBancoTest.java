package ar.edu.utn.dds.g08.entrega1;

import java.util.Arrays;

import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.junit.Test;

import static org.junit.Assert.*;

import org.uqbar.geodds.Point;

import ar.edu.utn.dds.g08.*;

public class SucursalBancoTest {
	Horario horario = new Horario(6, 7, 0, 0, 1, 0); // debe cambiar el horario 
	
	Servicio caja = new Servicio("Caja",Arrays.asList(horario));
	Servicio inversiones = new Servicio("Inversiones",Arrays.asList(horario));

	SucursalBanco sucBoca		= new SucursalBanco(new Direccion(new Point(-34.636648, -58.364657)), Arrays.asList(caja),"Galicia equipos");
	SucursalBanco sucRiber		= new SucursalBanco(new Direccion(new Point(-34.546465, -58.450952)), Arrays.asList(caja,inversiones), "Galicia equipos");
	SucursalBanco sucOlivos 	= new SucursalBanco(new Direccion(new Point(-34.510001, -58.506366)), Arrays.asList(caja,inversiones), "Galicia cheto");
	SucursalBanco sucAvellaneda = new SucursalBanco(new Direccion(new Point(-34.669773, -58.370346)), Arrays.asList(caja,inversiones), "Francés");
	SucursalBanco sucLugano		= new SucursalBanco(new Direccion(new Point(-34.659842, -58.468496)), Arrays.asList(caja,inversiones), "Francés");
	SucursalBanco sucPalermo	= new SucursalBanco(new Direccion(new Point(-34.591490, -58.434517)), Arrays.asList(caja,inversiones), "Francés");
	SucursalBanco sucPacheco	= new SucursalBanco(new Direccion(new Point(-34.449732, -58.662604)), Arrays.asList(caja), "ICBC");
	SucursalBanco sucSanMiguel	= new SucursalBanco(new Direccion(new Point(-34.542826, -58.711773)), Arrays.asList(caja), "ICBC");
	SucursalBanco sucAtlas 		= new SucursalBanco(new Direccion(new Point(-34.610236, -58.908390)), Arrays.asList(caja,inversiones), "Francés Go");
	SucursalBanco sucJCP 		= new SucursalBanco(new Direccion(new Point(-34.513290, -58.736901)), Arrays.asList(caja,inversiones), "Francés Go");

	@Test
	public void testCercania() {
		Point puntoCercano = new Point(-34.633313,-58.367634);
		
		assertTrue(sucBoca.esCercano(puntoCercano));

		puntoCercano = new Point(-34.544355,-58.450635);
		assertTrue(sucRiber.esCercano(puntoCercano));
		
		puntoCercano = new Point(-34.507449,-58.508000);
		assertTrue(sucOlivos.esCercano(puntoCercano));
		
		puntoCercano = new Point(-34.667153,-58.371609);
		assertTrue(sucAvellaneda.esCercano(puntoCercano));

		puntoCercano = new Point(-34.657573,-58.471702);
		assertTrue(sucLugano.esCercano(puntoCercano));

		Point puntoLejano = new Point(-34.603703,-58.381773);
		assertFalse(sucPalermo.esCercano(puntoLejano));
		assertFalse(sucPacheco.esCercano(puntoLejano));
		assertFalse(sucSanMiguel.esCercano(puntoLejano));
		assertFalse(sucAtlas.esCercano(puntoLejano));
		assertFalse(sucJCP.esCercano(puntoLejano));
	}

	@Test
	public void testDisponibilidad() {
		DateTime hora = new DateTime(2016, 07, 15, 14, 59);
		Instant horaDisponible = hora.toInstant();
		
		assertTrue(sucBoca.estaDisponible(horaDisponible));
		assertTrue(sucRiber.estaDisponible(horaDisponible));
		assertTrue(sucOlivos.estaDisponible(horaDisponible));
		assertTrue(sucAvellaneda.estaDisponible(horaDisponible));
		assertTrue(sucLugano.estaDisponible(horaDisponible));
		assertTrue(sucPalermo.estaDisponible(horaDisponible));
		assertTrue(sucPacheco.estaDisponible(horaDisponible));
		assertTrue(sucSanMiguel.estaDisponible(horaDisponible));
		assertTrue(sucAtlas.estaDisponible(horaDisponible));
		assertTrue(sucJCP.estaDisponible(horaDisponible));		

		hora = new DateTime(2016, 07, 15, 15, 01);
		Instant horaNODisponible = hora.toInstant();
		
		assertFalse(sucBoca.estaDisponible(horaNODisponible));
		assertFalse(sucRiber.estaDisponible(horaNODisponible));
		assertFalse(sucOlivos.estaDisponible(horaNODisponible));
		assertFalse(sucAvellaneda.estaDisponible(horaNODisponible));
		assertFalse(sucLugano.estaDisponible(horaNODisponible));
		assertFalse(sucPalermo.estaDisponible(horaNODisponible));
		assertFalse(sucPacheco.estaDisponible(horaNODisponible));
		assertFalse(sucSanMiguel.estaDisponible(horaNODisponible));
		assertFalse(sucAtlas.estaDisponible(horaNODisponible));
		assertFalse(sucJCP.estaDisponible(horaNODisponible));
	}

	@Test
	public void testBusqueda() {
		assertTrue(sucBoca.estaEnTexto("Galicia"));
		assertTrue(sucRiber.estaEnTexto("Galicia"));
		assertTrue(sucOlivos.estaEnTexto("Galicia"));
		assertTrue(sucAvellaneda.estaEnTexto("Francés"));
		assertTrue(sucLugano.estaEnTexto("Francés"));
		assertTrue(sucPalermo.estaEnTexto("Francés"));
		assertTrue(sucAtlas.estaEnTexto("Francés"));
		assertTrue(sucJCP.estaEnTexto("Francés"));
		assertTrue(sucPacheco.estaEnTexto("ICBC"));
		assertTrue(sucSanMiguel.estaEnTexto("IC"));

		assertFalse(sucBoca.estaEnTexto("Francés"));
		assertFalse(sucRiber.estaEnTexto("Francés"));
		assertFalse(sucOlivos.estaEnTexto("Francés"));
		assertFalse(sucAvellaneda.estaEnTexto("Galicia"));
		assertFalse(sucLugano.estaEnTexto("Galicia"));
		assertFalse(sucPalermo.estaEnTexto("Galicia"));
		assertFalse(sucAtlas.estaEnTexto("Galicia"));
		assertFalse(sucJCP.estaEnTexto("Galicia"));
		assertFalse(sucPacheco.estaEnTexto("H"));
		assertFalse(sucSanMiguel.estaEnTexto("H"));
	}

}
