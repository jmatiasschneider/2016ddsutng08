package ar.edu.utn.dds.g08.entrega1;

import java.util.Arrays;

import org.junit.Test;

import static org.junit.Assert.*;

import org.uqbar.geodds.Point;
import org.uqbar.geodds.Polygon;
import org.joda.time.*;

import ar.edu.utn.dds.g08.CGP;
import ar.edu.utn.dds.g08.Comuna;
import ar.edu.utn.dds.g08.Direccion;
import ar.edu.utn.dds.g08.Horario;
import ar.edu.utn.dds.g08.Servicio;

public class CGPTest {
	Point punto01 = new Point(-34.659074, -58.469602);
	Point punto02 = new Point(-34.657548, -58.467666);
	Point punto03 = new Point(-34.660019, -58.464501);
	Point punto04 = new Point(-34.663822, -58.469265);
	Point punto05 = new Point(-34.660535, -58.467513);

	Comuna comuna01 = new Comuna("Comuna 01", new Polygon(Arrays.asList(punto01, punto02, punto05)));
	Comuna comuna02 = new Comuna("Comuna 02", new Polygon(Arrays.asList(punto02, punto03, punto05)));
	Comuna comuna03 = new Comuna("Comuna 03", new Polygon(Arrays.asList(punto03, punto04, punto05)));

	Horario horario = new Horario(1, 5, 9, 0, 18, 0); // Lunes a viernes de 9 a
														// 18

	Servicio servicio = new Servicio("atención ciudadana",Arrays.asList(horario));
	Servicio servEspecial = new Servicio("Kiosko",Arrays.asList(new Horario(7, 7, 9, 0, 18, 0))); // Domingo

	CGP dep01Com01 = new CGP(new Direccion(new Point(-34.659079, -58.468545)),comuna01, Arrays.asList(servicio,servEspecial));

	CGP dep02Com01 = new CGP(new Direccion(new Point(-34.658364, -58.468245)),comuna01, Arrays.asList(servicio));

	CGP dep03Com01 = new CGP(new Direccion(new Point(-34.659873, -58.468084)),comuna01, Arrays.asList(servicio));

	CGP dep01Com02 = new CGP(new Direccion(new Point(-34.658549, -58.467118)),comuna02, Arrays.asList(servicio));

	CGP dep02Com02 = new CGP(new Direccion(new Point(-34.660411, -58.466989)),comuna02, Arrays.asList(servicio));

	CGP dep03Com02 = new CGP(new Direccion(new Point(-34.659802, -58.465541)),comuna02, Arrays.asList(servicio));

	CGP dep04Com02 = new CGP(new Direccion(new Point(-34.658919, -58.466217)),comuna02, Arrays.asList(servicio));

	CGP dep01Com03 = new CGP(new Direccion(new Point(-34.662518, -58.467803)),comuna03, Arrays.asList(servicio));

	CGP dep02Com03 = new CGP(new Direccion(new Point(-34.661018, -58.466977)),comuna03, Arrays.asList(servicio));

	CGP dep03Com03 = new CGP(new Direccion(new Point(-34.661194, -58.466140)),comuna03, Arrays.asList(servicio));

	@Test
	public void testCercania() {
		Point puntoCercano = new Point(-34.658953, -58.469015);

		assertTrue(dep01Com01.esCercano(puntoCercano));
		assertTrue(dep02Com01.esCercano(puntoCercano));
		assertTrue(dep03Com01.esCercano(puntoCercano));

		puntoCercano = new Point(-34.659034, -58.466672);
		assertTrue(dep01Com02.esCercano(puntoCercano));
		assertTrue(dep02Com02.esCercano(puntoCercano));
		assertTrue(dep03Com02.esCercano(puntoCercano));
		assertTrue(dep04Com02.esCercano(puntoCercano));

		puntoCercano = new Point(-34.662175, -58.467404);
		assertTrue(dep01Com03.esCercano(puntoCercano));
		assertTrue(dep02Com03.esCercano(puntoCercano));
		assertTrue(dep03Com03.esCercano(puntoCercano));

		Point puntoLejano = new Point(-34.636926, -58.478617);
		assertFalse(dep01Com01.esCercano(puntoLejano));
		assertFalse(dep02Com01.esCercano(puntoLejano));
		assertFalse(dep03Com01.esCercano(puntoLejano));

		assertFalse(dep01Com02.esCercano(puntoLejano));
		assertFalse(dep02Com02.esCercano(puntoLejano));
		assertFalse(dep03Com02.esCercano(puntoLejano));
		assertFalse(dep04Com02.esCercano(puntoLejano));

		assertFalse(dep01Com03.esCercano(puntoLejano));
		assertFalse(dep02Com03.esCercano(puntoLejano));
		assertFalse(dep03Com03.esCercano(puntoLejano));
	}

	@Test
	public void testDisponibilidad() {
		DateTime hora = new DateTime(2016, 07, 15, 15, 00);
		Instant horaDisponible = hora.toInstant();
		assertFalse(dep01Com01.estaDisponible(horaDisponible,"Kiosko"));
		assertTrue(dep01Com01.estaDisponible(horaDisponible));
		assertTrue(dep01Com01.estaDisponible(horaDisponible,"atención ciudadana"));
		assertTrue(dep02Com01.estaDisponible(horaDisponible));
		assertTrue(dep03Com01.estaDisponible(horaDisponible));

		assertTrue(dep01Com02.estaDisponible(horaDisponible));
		assertTrue(dep02Com02.estaDisponible(horaDisponible));
		assertTrue(dep03Com02.estaDisponible(horaDisponible));
		assertTrue(dep04Com02.estaDisponible(horaDisponible));

		assertTrue(dep01Com03.estaDisponible(horaDisponible));
		assertTrue(dep02Com03.estaDisponible(horaDisponible));
		assertTrue(dep03Com03.estaDisponible(horaDisponible));
		
		hora = new DateTime(2016, 07, 17, 15, 00);
		Instant horaNODisponible = hora.toInstant();
		assertTrue(dep01Com01.estaDisponible(horaNODisponible));
		assertFalse(dep01Com01.estaDisponible(horaNODisponible,"atención ciudadana"));
		assertTrue(dep01Com01.estaDisponible(horaNODisponible,"Kiosko"));
		assertFalse(dep02Com01.estaDisponible(horaNODisponible));
		assertFalse(dep03Com01.estaDisponible(horaNODisponible));

		assertFalse(dep01Com02.estaDisponible(horaNODisponible));
		assertFalse(dep02Com02.estaDisponible(horaNODisponible));
		assertFalse(dep03Com02.estaDisponible(horaNODisponible));
		assertFalse(dep04Com02.estaDisponible(horaNODisponible));

		assertFalse(dep01Com03.estaDisponible(horaNODisponible));
		assertFalse(dep02Com03.estaDisponible(horaNODisponible));
		assertFalse(dep03Com03.estaDisponible(horaNODisponible));
	}

	@Test
	public void testBusqueda() {
		assertTrue(dep01Com01.estaEnTexto("Comuna 01"));
		assertTrue(dep01Com01.estaEnTexto("atención"));
		assertTrue(dep02Com01.estaEnTexto("Comuna 01"));
		assertTrue(dep02Com01.estaEnTexto("atención"));
		assertTrue(dep03Com01.estaEnTexto("Comuna 01"));
		assertTrue(dep03Com01.estaEnTexto("atención"));

		assertTrue(dep01Com02.estaEnTexto("Comuna 02"));
		assertTrue(dep01Com02.estaEnTexto("atención"));
		assertTrue(dep02Com02.estaEnTexto("Comuna 02"));
		assertTrue(dep02Com02.estaEnTexto("atención"));
		assertTrue(dep03Com02.estaEnTexto("Comuna 02"));
		assertTrue(dep03Com02.estaEnTexto("atención"));
		assertTrue(dep04Com02.estaEnTexto("Comuna 02"));
		assertTrue(dep04Com02.estaEnTexto("atención"));

		assertTrue(dep01Com03.estaEnTexto("Comuna 03"));
		assertTrue(dep01Com03.estaEnTexto("atención"));
		assertTrue(dep02Com03.estaEnTexto("Comuna 03"));
		assertTrue(dep02Com03.estaEnTexto("atención"));
		assertTrue(dep03Com03.estaEnTexto("Comuna 03"));
		assertTrue(dep03Com03.estaEnTexto("atención"));

		assertFalse(dep01Com01.estaEnTexto("tp diseño"));
		assertFalse(dep02Com01.estaEnTexto("tp diseño"));
		assertFalse(dep03Com01.estaEnTexto("tp diseño"));

		assertFalse(dep01Com02.estaEnTexto("tp diseño"));
		assertFalse(dep02Com02.estaEnTexto("tp diseño"));
		assertFalse(dep03Com02.estaEnTexto("tp diseño"));
		assertFalse(dep04Com02.estaEnTexto("tp diseño"));

		assertFalse(dep01Com03.estaEnTexto("tp diseño"));
		assertFalse(dep02Com03.estaEnTexto("tp diseño"));
		assertFalse(dep03Com03.estaEnTexto("tp diseño"));
	}
}
