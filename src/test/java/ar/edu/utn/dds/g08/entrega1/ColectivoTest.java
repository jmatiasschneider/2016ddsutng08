package ar.edu.utn.dds.g08.entrega1;

import ar.edu.utn.dds.g08.*;

import java.util.Arrays;
import java.util.List;

import org.joda.time.Instant;
import org.junit.Test;

import static org.junit.Assert.*;

import org.uqbar.geodds.Point;

public class ColectivoTest {
	Parada paradaCampus = new Parada("icono parada", new Direccion(new Point(
			-34.659932, -58.468362)));
	Parada paradaRetiro = new Parada("icono parada", new Direccion(new Point(
			-34.591678, -58.374675)));
	Parada paradaConstitucion = new Parada("icono parada", new Direccion(
			new Point(-34.627820, -58.380990)));
	Parada paradaBoedo = new Parada("icono parada", new Direccion(new Point(
			-34.625111, -58.416162)));
	Parada paradaDevoto = new Parada("icono parada", new Direccion(new Point(
			-34.600290, -58.511885)));
	Parada paradaPalermo = new Parada("icono parada", new Direccion(new Point(
			-34.578355, -58.425631)));
	Parada paradaPteSaavedra = new Parada("icono parada", new Direccion(
			new Point(-34.537468, -58.476507)));
	Parada paradaMedrano = new Parada("icono parada", new Direccion(new Point(
			-34.598552, -58.420274)));

	List<Parada> paradas114 = Arrays.asList(paradaCampus, paradaDevoto);
	List<Parada> paradas101 = Arrays.asList(paradaCampus, paradaConstitucion, paradaRetiro);
	List<Parada> paradas152 = Arrays.asList(paradaRetiro, paradaPalermo,
			paradaPteSaavedra);
	List<Parada> paradas115 = Arrays.asList(paradaBoedo, paradaConstitucion,
			paradaRetiro, paradaPalermo, paradaPteSaavedra);
	List<Parada> paradas55 = Arrays.asList(paradaBoedo, paradaPalermo);
	List<Parada> paradas60 = Arrays.asList(paradaConstitucion, paradaRetiro,
			paradaPalermo, paradaPteSaavedra);
	List<Parada> paradas71 = Arrays.asList(paradaPteSaavedra, paradaMedrano,
			paradaPalermo);
	List<Parada> paradas160 = Arrays.asList(paradaRetiro, paradaMedrano,
			paradaPalermo, paradaPteSaavedra);
	List<Parada> paradas7 = Arrays.asList(paradaCampus, paradaConstitucion,
			paradaRetiro, paradaPalermo);
	List<Parada> paradasTotal = Arrays.asList(paradaCampus, paradaBoedo,
			paradaConstitucion, paradaRetiro, paradaMedrano, paradaPalermo,
			paradaDevoto, paradaPteSaavedra);

	Colectivo bondi114 = new Colectivo("icono 114", new Direccion(new Point(
			-34.000000, -58.000000)), "114", paradas114);
	Colectivo bondi101 = new Colectivo("icono 101", new Direccion(new Point(
			-34.000000, -58.000000)), "101", paradas101);
	Colectivo bondi152 = new Colectivo("icono 152", new Direccion(new Point(
			-34.000000, -58.000000)), "152", paradas152);
	Colectivo bondi115 = new Colectivo("icono 115", new Direccion(new Point(
			-34.000000, -58.000000)), "115", paradas115);
	Colectivo bondi55 = new Colectivo("icono 55", new Direccion(new Point(
			-34.000000, -58.000000)), "55", paradas55);
	Colectivo bondi60 = new Colectivo("icono 60", new Direccion(new Point(
			-34.000000, -58.000000)), "60", paradas60);
	Colectivo bondi71 = new Colectivo("icono 71", new Direccion(new Point(
			-34.000000, -58.000000)), "71", paradas71);
	Colectivo bondi160 = new Colectivo("icono 160", new Direccion(new Point(
			-34.000000, -58.000000)), "160", paradas160);
	Colectivo bondi7 = new Colectivo("icono 7", new Direccion(new Point(
			-34.000000, -58.000000)), "7", paradas7);
	Colectivo bondiTotal = new Colectivo("icono Total", new Direccion(
			new Point(-34.000000, -58.000000)), "Total", paradasTotal);

	@Test
	public void testCercania() {
		Point puntoCercano = new Point(-34.591884,-58.374515); //retiro
		
		assertTrue(bondi101.esCercano(puntoCercano));
		assertTrue(bondi152.esCercano(puntoCercano));
		assertTrue(bondi7.esCercano(puntoCercano));
		assertTrue(bondiTotal.esCercano(puntoCercano));
		assertTrue(bondi60.esCercano(puntoCercano));
		assertTrue(bondi160.esCercano(puntoCercano));
		assertTrue(bondi115.esCercano(puntoCercano));

		puntoCercano = new Point(-34.578453,-58.425047);
		assertTrue(bondi71.esCercano(puntoCercano));
		assertTrue(bondi55.esCercano(puntoCercano));

		puntoCercano = new Point(-34.660192,-58.468018);
		assertTrue(bondi114.esCercano(puntoCercano));
		
		Point puntoLejano = new Point(-34.636926, -58.478617);
		assertFalse(bondi114.esCercano(puntoLejano));
		assertFalse(bondi101.esCercano(puntoLejano));
		assertFalse(bondi152.esCercano(puntoLejano));
		assertFalse(bondi55.esCercano(puntoLejano));
		assertFalse(bondi115.esCercano(puntoLejano));
		assertFalse(bondi60.esCercano(puntoLejano));
		assertFalse(bondi71.esCercano(puntoLejano));
		assertFalse(bondi160.esCercano(puntoLejano));
		assertFalse(bondi7.esCercano(puntoLejano));
		assertFalse(bondiTotal.esCercano(puntoLejano));

	}

	@Test
	public void testDisponibilidad() {
		// en cualquier horario es verdadero
		Instant horaDisponible = Instant.now();

		assertTrue(bondi114.estaDisponible(horaDisponible));
		assertTrue(bondi101.estaDisponible(horaDisponible));
		assertTrue(bondi152.estaDisponible(horaDisponible));
		assertTrue(bondi115.estaDisponible(horaDisponible));
		assertTrue(bondi55.estaDisponible(horaDisponible));
		assertTrue(bondi60.estaDisponible(horaDisponible));
		assertTrue(bondi160.estaDisponible(horaDisponible));
		assertTrue(bondi7.estaDisponible(horaDisponible));
		assertTrue(bondiTotal.estaDisponible(horaDisponible));
		assertTrue(bondi71.estaDisponible(horaDisponible));

	}

	@Test
	public void testBusqueda() {
		assertTrue(bondi114.estaEnTexto("114"));
		assertFalse(bondi114.estaEnTexto("1140"));

		assertTrue(bondi60.estaEnTexto("60"));
		assertFalse(bondi60.estaEnTexto("600"));

		assertTrue(bondi160.estaEnTexto("160"));
		assertFalse(bondi160.estaEnTexto("160h"));

		assertTrue(bondi7.estaEnTexto("7"));
		assertFalse(bondi7.estaEnTexto(" 7"));

		assertTrue(bondiTotal.estaEnTexto("Total"));
		assertFalse(bondiTotal.estaEnTexto("Totál"));

		assertTrue(bondi71.estaEnTexto("71"));
		assertFalse(bondi71.estaEnTexto("71 "));

		assertTrue(bondi55.estaEnTexto("55"));
		assertFalse(bondi55.estaEnTexto("5"));

		assertTrue(bondi115.estaEnTexto("115"));
		assertFalse(bondi115.estaEnTexto("11"));

		assertTrue(bondi152.estaEnTexto("152"));
		assertFalse(bondi152.estaEnTexto("15"));

		assertTrue(bondi101.estaEnTexto("101"));
		assertFalse(bondi101.estaEnTexto("1014"));

	}

}