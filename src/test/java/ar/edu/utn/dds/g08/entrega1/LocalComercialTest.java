package ar.edu.utn.dds.g08.entrega1;

import org.uqbar.geodds.Point;

import ar.edu.utn.dds.g08.*;

import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.Arrays;

public class LocalComercialTest {
	Rubro jugueterias = new Rubro("Juguetería", 1);
	Rubro comercioAlim = new Rubro("Alimentos", 0.1);
	Rubro indumentaria = new Rubro("Indumentaria", 5);

	Horario findes = new Horario(6, 7, 10, 00, 19, 00);
	Horario entreSem = new Horario(1, 5, 10, 00, 19, 00);
	Horario viernes = new Horario(5, 5, 10, 00, 19, 00);

	LocalComercial jugueteria1 = new LocalComercial("icono",
													new Direccion(new Point(-34.603708, -58.381596)),
													jugueterias,
													Arrays.asList("Buzz", "Quija"),
													Arrays.asList(findes,viernes),
													"Carrousell");
	
	LocalComercial jugueteria2 = new LocalComercial("icono",
													new Direccion(new Point(-34.600670, -58.389589)),
													jugueterias,
													Arrays.asList("Oca", "Buzz"),
													Arrays.asList(findes),
													"Dalesandro");
	
	LocalComercial carniceria1 = new LocalComercial("icono",
													new Direccion(new Point(-34.518606, -58.738618)),
													comercioAlim,
													Arrays.asList("Bondiola", "Asado"),
													Arrays.asList(entreSem),
													"Maxi");
	
	LocalComercial carniceria2 = new LocalComercial("icono",
													new Direccion(new Point(-34.518056, -58.739448)),
													comercioAlim,
													Arrays.asList("Bondiola", "Asado"),
													Arrays.asList(entreSem,findes),
													"Rioplatense");
	
	LocalComercial carniceria3 = new LocalComercial("icono",
													new Direccion(new Point(-34.514562, -58.742641)),
													comercioAlim,
													Arrays.asList("Bondiola", "Pollo", "Vacío"),
													Arrays.asList(entreSem,findes),
													"Rey");
	
	LocalComercial hiper1 = new LocalComercial("icono",
												new Direccion(new Point(-34.531739, -58.702492)),
												comercioAlim,
												Arrays.asList("Pollo","leche","Pan"),
												Arrays.asList(entreSem),
												"Coto");
	
	LocalComercial hiper2 = new LocalComercial("icono",
												new Direccion(new Point(-34.535464, -58.704859)),
												comercioAlim,
												Arrays.asList("Leche","Pan","Lavandina"),
												Arrays.asList(findes,viernes),
												"Chino");
	
	LocalComercial hiper3 = new LocalComercial("icono",
												new Direccion(new Point(-34.540439, -58.708562)),
												comercioAlim,
												Arrays.asList("Cerveza","Servilleta","Ajo"),
												Arrays.asList(entreSem,findes),
												"Jumbo");
	
	LocalComercial hiper4 = new LocalComercial("icono",
												new Direccion(new Point(-34.546163, -58.706680)),
												comercioAlim,
												Arrays.asList("Pan","Coca"),
												Arrays.asList(entreSem,findes),
												"Vea");
	
	LocalComercial ropa1 = new LocalComercial("icono",
												new Direccion(new Point(-34.589894, -58.430139)),
												indumentaria,
												Arrays.asList("Jean","Camisa"),
												Arrays.asList(findes,viernes),
												"Herencia");

	@Test
	public void testCercania() {
		Point puntoCercano = new Point(-34.599000,-58.381320);
		Direccion dir = jugueteria1.getDireccion();
		
		assertTrue(dir.distancia(puntoCercano)>0.5);
		assertTrue(jugueteria1.esCercano(puntoCercano));
		assertTrue(jugueteria2.esCercano(puntoCercano));

		puntoCercano = new Point(-34.518620, -58.738620);
		assertTrue(carniceria1.esCercano(puntoCercano));
		assertTrue(carniceria2.esCercano(puntoCercano));
		
		puntoCercano = new Point(-34.514960, -58.742405);
		assertTrue(carniceria3.esCercano(puntoCercano));

		puntoCercano = new Point(-34.531774, -58.701720);
		assertTrue(hiper1.esCercano(puntoCercano));
		
		puntoCercano = new Point(-34.535022, -58.704365);
		assertTrue(hiper2.esCercano(puntoCercano));
		
		puntoCercano = new Point(-34.540289, -58.708948);
		assertTrue(hiper3.esCercano(puntoCercano));
		
		puntoCercano = new Point(-34.545756, -58.707163);
		assertTrue(hiper4.esCercano(puntoCercano));
		
		puntoCercano = new Point(-34.598504, -58.420285);
		dir = ropa1.getDireccion();
		assertTrue(dir.distancia(puntoCercano)>1);
		assertTrue(ropa1.esCercano(puntoCercano));
		
		Point puntoLejano = new Point(-34.530084,-58.698525);
		assertFalse(jugueteria1.esCercano(puntoLejano));
		assertFalse(jugueteria2.esCercano(puntoLejano));
		assertFalse(ropa1.esCercano(puntoLejano));

		dir = hiper1.getDireccion();
		assertTrue(dir.distancia(puntoLejano)<0.5);
		assertFalse(hiper1.esCercano(puntoLejano));
		assertFalse(hiper2.esCercano(puntoLejano));
		assertFalse(hiper3.esCercano(puntoLejano));
		assertFalse(hiper4.esCercano(puntoLejano));
		assertFalse(carniceria1.esCercano(puntoLejano));
		assertFalse(carniceria2.esCercano(puntoLejano));
		assertFalse(carniceria3.esCercano(puntoLejano));
	}

	@Test
	public void testDisponibilidad() {
		DateTime hora = new DateTime(2016, 07, 15, 14, 59);
		Instant horaDisponible = hora.toInstant();
		
		assertFalse(jugueteria2.estaDisponible(horaDisponible));
		assertTrue(jugueteria1.estaDisponible(horaDisponible));
		assertTrue(carniceria1.estaDisponible(horaDisponible));
		assertTrue(carniceria2.estaDisponible(horaDisponible));
		assertTrue(carniceria3.estaDisponible(horaDisponible));
		assertTrue(hiper1.estaDisponible(horaDisponible));
		assertTrue(hiper2.estaDisponible(horaDisponible));
		assertTrue(hiper3.estaDisponible(horaDisponible));
		assertTrue(hiper4.estaDisponible(horaDisponible));
		assertTrue(ropa1.estaDisponible(horaDisponible));
		
		hora = new DateTime(2016, 07, 15, 23, 59);
		Instant horaNODisponible = hora.toInstant();
		assertFalse(jugueteria1.estaDisponible(horaNODisponible));
		assertFalse(jugueteria2.estaDisponible(horaNODisponible));
		assertFalse(hiper1.estaDisponible(horaNODisponible));
		assertFalse(hiper2.estaDisponible(horaNODisponible));
		assertFalse(hiper3.estaDisponible(horaNODisponible));
		assertFalse(hiper4.estaDisponible(horaNODisponible));
		assertFalse(carniceria1.estaDisponible(horaNODisponible));
		assertFalse(carniceria2.estaDisponible(horaNODisponible));
		assertFalse(carniceria3.estaDisponible(horaNODisponible));
		assertFalse(ropa1.estaDisponible(horaNODisponible));

		hora = new DateTime(2016, 07, 16, 13, 59);
		horaDisponible = hora.toInstant();
		assertTrue(jugueteria2.estaDisponible(horaDisponible));
	}

	@Test
	public void testBusqueda() {
		assertTrue(jugueteria1.estaEnTexto("Juguetería"));
		assertFalse(jugueteria1.estaEnTexto("Jugueterías"));
		assertTrue(jugueteria1.estaEnTexto("Buzz"));
		assertFalse(jugueteria1.estaEnTexto("Oca"));
		assertTrue(jugueteria1.estaEnTexto("Carro"));
		assertFalse(jugueteria1.estaEnTexto("Carros"));

		assertTrue(jugueteria2.estaEnTexto("Juguetería"));
		assertFalse(jugueteria2.estaEnTexto("Jugueterías"));
		assertTrue(jugueteria2.estaEnTexto("Buzz"));
		assertFalse(jugueteria2.estaEnTexto("Ocas"));
		assertTrue(jugueteria2.estaEnTexto("Dales"));
		assertFalse(jugueteria2.estaEnTexto("D'ales"));

		assertTrue(carniceria1.estaEnTexto("Alimentos"));
		assertFalse(carniceria1.estaEnTexto("Juguetería"));
		assertTrue(carniceria1.estaEnTexto("Asado"));
		assertFalse(carniceria1.estaEnTexto("Cerdo"));
		assertTrue(carniceria1.estaEnTexto("Max"));
		assertFalse(carniceria1.estaEnTexto("Mss"));

		assertTrue(carniceria2.estaEnTexto("Alimentos"));
		assertFalse(carniceria2.estaEnTexto("Juguetería"));
		assertTrue(carniceria2.estaEnTexto("Asado"));
		assertFalse(carniceria2.estaEnTexto("Cerdo"));
		assertTrue(carniceria2.estaEnTexto("Rio"));
		assertFalse(carniceria2.estaEnTexto("Max"));

		assertTrue(carniceria3.estaEnTexto("Alimentos"));
		assertFalse(carniceria3.estaEnTexto("Juguetería"));
		assertTrue(carniceria3.estaEnTexto("Bondiola"));
		assertFalse(carniceria3.estaEnTexto("Asado"));
		assertTrue(carniceria3.estaEnTexto("R"));
		assertFalse(carniceria3.estaEnTexto("M"));

		assertTrue(hiper1.estaEnTexto("Alimentos"));
		assertFalse(hiper1.estaEnTexto("Juguetería"));
		assertTrue(hiper1.estaEnTexto("leche"));
		assertFalse(hiper1.estaEnTexto("Asado"));
		assertTrue(hiper1.estaEnTexto("C"));
		assertFalse(hiper1.estaEnTexto("M"));

		assertTrue(hiper2.estaEnTexto("Alimentos"));
		assertFalse(hiper2.estaEnTexto("Juguetería"));
		assertTrue(hiper2.estaEnTexto("Leche"));
		assertFalse(hiper2.estaEnTexto("Asado"));
		assertTrue(hiper2.estaEnTexto("C"));
		assertFalse(hiper2.estaEnTexto("M"));

		assertTrue(hiper3.estaEnTexto("Alimentos"));
		assertFalse(hiper3.estaEnTexto("Juguetería"));
		assertTrue(hiper3.estaEnTexto("Ajo"));
		assertFalse(hiper3.estaEnTexto("Asado"));
		assertTrue(hiper3.estaEnTexto("Jumbo"));
		assertFalse(hiper3.estaEnTexto("Coto"));
		
		assertTrue(hiper4.estaEnTexto("Alimentos"));
		assertFalse(hiper4.estaEnTexto("Juguetería"));
		assertTrue(hiper4.estaEnTexto("Pan"));
		assertFalse(hiper4.estaEnTexto("Asado"));
		assertTrue(hiper4.estaEnTexto("Vea"));
		assertFalse(hiper4.estaEnTexto("Coto"));

		assertTrue(ropa1.estaEnTexto("Indumentaria"));
		assertFalse(ropa1.estaEnTexto("Juguetería"));
		assertTrue(ropa1.estaEnTexto("Jean"));
		assertFalse(ropa1.estaEnTexto("Pantalón"));
		assertTrue(ropa1.estaEnTexto("Her"));
		assertFalse(ropa1.estaEnTexto("Coto"));
		
	}
}
