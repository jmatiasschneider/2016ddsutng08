package ar.edu.utn.dds.g08;

import static org.junit.Assert.*;


import java.util.Arrays;

import org.junit.Test;
import org.uqbar.geodds.Point;
import org.uqbar.geodds.Polygon;

public class TestEntrega1 {
/*
	private PuntoDeInteres unPunto;
	private Point otroPunto = new Point(-34.631572, -58.483537);
	private Point otroPuntoCercaBondiCerca = new Point(-34.631042, -58.482634);
	private Point otroPuntoCercaBondiLejos = new Point(-36.631042, -58.482634);

	// Puntos Cardinales instancias:

	private Point noroeste1 = new Point(-35.631121, -59.485500);
	private Point suroeste1 = new Point(-35.632534, -59.484288);
	private Point sureste1 = new Point(-35.632384, -59.482678);
	private Point noreste1 = new Point(-35.631042, -59.482635);
	//

	private Point noroeste2 = new Point(-36.631121, -60.485500);
	private Point suroeste2 = new Point(-36.632534, -60.484288);
	private Point sureste2 = new Point(-36.632384, -60.482678);
	private Point noreste2 = new Point(-36.631042, -60.482635);
	//

	private Point noroeste3 = new Point(-37.631121, -60.485500);
	private Point suroeste3 = new Point(-37632534, -60.484288);
	private Point sureste3 = new Point(-37.632384, -60.482678);
	private Point noreste3 = new Point(-37.631042, -60.482635);
	//

	private Point noroeste4 = new Point(-30.631121, -58.485500);
	private Point suroeste4 = new Point(-30.632534, -58.484288);
	private Point sureste4 = new Point(-30.632384, -58.482678);
	private Point noreste4 = new Point(-30.631042, -58.482635);
	//

	private Point noroeste5 = new Point(-34.631121, -58.485500);
	private Point suroeste5 = new Point(-34.632534, -58.484288);
	private Point sureste5 = new Point(-34.632384, -58.482678);
	private Point noreste5 = new Point(-34.631042, -58.482635);

	// Colectivos instancias

	private Colectivo bondi1 = new Colectivo("1", Arrays.asList(noroeste1,
			suroeste1, sureste1, noreste1));
	private Colectivo bondi2 = new Colectivo("2", Arrays.asList(noroeste2,
			suroeste2, sureste2, noreste2));
	private Colectivo bondi3 = new Colectivo("3", Arrays.asList(noroeste3,
			suroeste3, sureste3, noreste3));
	private Colectivo bondi4 = new Colectivo("4", Arrays.asList(noroeste4,
			suroeste4, sureste4, noreste4));
	private Colectivo bondi5 = new Colectivo("5", Arrays.asList(noroeste5,
			suroeste5, sureste5, noreste5));

	// CGP instancias

	Comuna c1 = new Comuna("comuna1", new Polygon(Arrays.asList(noroeste5,
			suroeste5, sureste5, noreste5)));
	Servicio legales = new Servicio("Legales", new Horario(1, 5, 8, 18, 0, 0));
	private CGP cgp1 = new CGP(c1, Arrays.asList(legales));

	Comuna c2 = new Comuna("comuna2", new Polygon(Arrays.asList(noroeste5,
			suroeste5, sureste5, noreste5)));
	Servicio def_consumidor = new Servicio("Defensa al Consumidor",
			new Horario(1, 5, 8, 18, 0, 0));
	private CGP cgp2 = new CGP(c2, Arrays.asList(legales));

	Comuna c3 = new Comuna("comuna3", new Polygon(Arrays.asList(noroeste5,
			suroeste5, sureste5, noreste5)));
	Servicio asesoramiento = new Servicio("Asesoramiento", new Horario(1, 5, 8,
			18, 0, 0));
	private CGP cgp3 = new CGP(c3, Arrays.asList(legales));

	Comuna c4 = new Comuna("comuna4", new Polygon(Arrays.asList(noroeste5,
			suroeste5, sureste5, noreste5)));
	Servicio civico = new Servicio("Civicos", new Horario(1, 5, 8, 18, 0, 0));
	private CGP cgp4 = new CGP(c4, Arrays.asList(legales));

	Comuna c5 = new Comuna("comuna5", new Polygon(Arrays.asList(noroeste5,
			suroeste5, sureste5, noreste5)));
	Servicio sociales = new Servicio("Sociales", new Horario(1, 5, 8, 18, 0, 0));
	private CGP cgp5 = new CGP(c5, Arrays.asList(legales));

	// Locales comerciales instancias

	Point ubicacionKiosco = new Point(-34.631042, -58.482635);
	Point ubicacionFarmacia = new Point(-34.631042, -58.482635);
	Point ubicacionFerreteria = new Point(-34.631042, -58.482635);
	Point ubicacionBazar = new Point(-34.631042, -58.482635);
	Point ubicacionSupermercado = new Point(-34.631042, -58.482635);

	private Rubro kioskoDiario = new Rubro("kioskoDiario", 0.2);
	private LocalComercial KioskoCarlitos = new LocalComercial(kioskoDiario,
			"diario", Arrays.asList(new Horario(1, 5, 8, 0, 13, 00),
					new Horario(1, 5, 14, 0, 23, 00)), "Kiosco De Carlitos",
			ubicacionKiosco);

	private Rubro farmacia = new Rubro("farmacia", 0.4);
	private LocalComercial farmaciaPepe = new LocalComercial(farmacia,
			"medicamento", Arrays.asList(new Horario(1, 5, 8, 00, 20, 0)),
			"Farmacia Pepe", ubicacionFarmacia);

	private Rubro ferreteria = new Rubro("ferreteria", 0.5);
	private LocalComercial ferreteriaPablito = new LocalComercial(ferreteria,
			"martillo", Arrays.asList(new Horario(1, 5, 8, 0, 21, 0)),
			"Ferreteria Pablito", ubicacionFerreteria);

	private Rubro bazar = new Rubro("bazar", 0.2);
	private LocalComercial bazarPanchos = new LocalComercial(bazar, "lamparas",
			Arrays.asList(new Horario(1, 5, 8, 0, 11, 0)),
			"Bazar Lo de Pancho", ubicacionBazar);

	private Rubro supermercado = new Rubro("supermercado", 0.6);
	private LocalComercial carrefour = new LocalComercial(supermercado,
			"comida", Arrays.asList(new Horario(1, 5, 8, 0, 15, 0)),
			"carrefour", ubicacionSupermercado);

	// Bancos

	private SucursalBanco bancoPatagonia = new SucursalBanco("depositos",
			"Patagonia", otroPunto);

	// --------------------------------------------
	// Tests Cercania

	// bondis

	@Test
	public void noEsCercanoColectivo1() {
		assertFalse(this.bondi1.esCercano(otroPuntoCercaBondiLejos));
	}

	public void esCercanoColectivo2() {
		assertTrue(this.bondi2.esCercano(otroPuntoCercaBondiCerca));
	}

	@Test
	public void noEsCercanoColectivo2() {
		assertFalse(this.bondi2.esCercano(otroPuntoCercaBondiLejos));
	}

	public void esCercanoColectivo3() {
		assertTrue(this.bondi3.esCercano(otroPuntoCercaBondiCerca));
	}

	@Test
	public void noEsCercanoColectivo3() {
		assertFalse(this.bondi3.esCercano(otroPuntoCercaBondiLejos));
	}

	public void esCercanoColectivo4() {
		assertTrue(this.bondi4.esCercano(otroPuntoCercaBondiCerca));
	}

	@Test
	public void noEsCercanoColectivo4() {
		assertFalse(this.bondi4.esCercano(otroPuntoCercaBondiLejos));
	}

	public void esCercanoColectivo5() {
		assertTrue(this.bondi5.esCercano(otroPuntoCercaBondiCerca));
	}

	@Test
	public void noEsCercanoColectivo5() {
		assertFalse(this.bondi5.esCercano(otroPuntoCercaBondiLejos));
	}

	// Comunas

	@Test
	public void esCercanoComuna1() {
		assertTrue(this.cgp1.esCercano(otroPunto));
	}

	@Test
	public void esCercanoComuna2() {
		assertTrue(this.cgp2.esCercano(otroPunto));
	}

	@Test
	public void esCercanoComuna3() {
		assertTrue(this.cgp3.esCercano(otroPunto));
	}

	@Test
	public void esCercanoComuna4() {
		assertTrue(this.cgp4.esCercano(otroPunto));
	}

	@Test
	public void esCercanoComuna5() {
		assertTrue(this.cgp5.esCercano(otroPunto));
	}

	@Test
	public void noEsPuntoCercano() {
		this.unPunto = new PuntoDeInteres();
		this.unPunto.setUbicacion(new Point(0, 0));
		assertFalse(this.unPunto.esCercano(otroPunto));
	}

	// Locales comerciales

	@Test
	public void esCercanoLocalComercial1() {

		assertTrue(this.KioskoCarlitos.esCercano(otroPuntoCercaBondiCerca));
	}

	@Test
	public void esCercanoLocalComercial2() {
		assertTrue(this.farmaciaPepe.esCercano(otroPuntoCercaBondiCerca));
	}

	@Test
	public void esCercanoLocalComercial3() {
		assertTrue(this.ferreteriaPablito.esCercano(otroPuntoCercaBondiCerca));
	}

	@Test
	public void esCercanoLocalComercial4() {
		assertTrue(this.bazarPanchos.esCercano(otroPuntoCercaBondiCerca));
	}

	@Test
	public void esCercanoLocalComercial5() {
		assertTrue(this.carrefour.esCercano(otroPuntoCercaBondiCerca));
	}

	// Test Disponibilidad

	// Comerciales

	@Test
	public void noEstaDsiponibleComercial() {
		assertTrue(this.KioskoCarlitos.estaDisponible(2016, 4, 4, 15, 00, 00));
	}

	// Bondis
	@Test
	public void estaDisponibleBondi() {
		assertTrue(this.bondi1.estaDisponible(2016, 04, 4, 13, 10, 00));
	}

	@Test
	public void estaDsiponibleBanco() {
		assertTrue(this.bancoPatagonia.estaDisponible(2016, 4, 4, 13, 10, 1));
	}

	@Test
	public void noEstaDsiponibleBanco() {
		assertFalse(this.bancoPatagonia.estaDisponible(2016, 04, 4, 20, 10, 00));
	}

	// Test estaEnTexto

	// Banco
	@Test
	public void bancoEstaEnTextoPoNombre() {
		assertTrue(this.bancoPatagonia.estaEnTexto("Patagonia"));
	}

	@Test
	public void bancoNoEstaEnTextoPoNombre() {
		assertFalse(this.bancoPatagonia.estaEnTexto("Kio"));
	}

	// LocalComercial
	@Test
	public void localComercialEstaEnTextoPoNombre() {
		assertTrue(this.KioskoCarlitos.estaEnTexto("Kio"));
	}

	@Test
	public void localComercialNoEstaEnTextoPoNombre() {
		assertFalse(this.KioskoCarlitos.estaEnTexto("lala"));
	}

	@Test
	public void localComercialEstaEnTextoPorPalabraClave() {
		assertTrue(this.KioskoCarlitos.estaEnTexto("diario"));
	}

	@Test
	public void localComercialNoEstaEnTextoPorPalabraClave() {
		assertFalse(this.KioskoCarlitos.estaEnTexto("CocaCola"));
	}

	@Test
	public void localComercialEstaEnTextoPorRubroe() {
		assertTrue(this.KioskoCarlitos.estaEnTexto("kioskoDiario"));
	}

	@Test
	public void localComercialNoEstaEnTextoPorRubro() {
		assertFalse(this.KioskoCarlitos.estaEnTexto("supermercado"));
	}

	// Buses
	@Test
	public void BondiEstaEnTextoPoNombre() {
		assertTrue(this.bondi1.estaEnTexto("1"));
	}

	@Test
	public void BondiNoEstaEnTextoPoNombre() {
		assertFalse(this.bondi1.estaEnTexto("2"));
	}

	// CGP
	@Test
	public void cgpEstaEnTextoPorNro() {
		assertTrue(this.cgp1.estaEnTexto("comuna1"));
	}

	@Test
	public void cgpNoEstaEnTextoPorNro() {
		assertFalse(this.cgp1.estaEnTexto("comuna2"));
	}

	@Test
	public void cgpEstaEnTextoPorServicio() {
		assertTrue(this.cgp1.estaEnTexto("Legales"));
	}

	@Test
	public void cgpNoEstaEnTextoPorServicio() {
		assertFalse(this.cgp1.estaEnTexto("civil"));
	}
*/}
