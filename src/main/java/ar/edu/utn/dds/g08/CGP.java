package ar.edu.utn.dds.g08;

import java.util.List;

import org.joda.time.Instant;
import org.uqbar.geodds.*;

public class CGP extends PuntoDeInteres {

	private Comuna comuna;
	protected List<Servicio> servicios;

	public CGP(Direccion direccion, Comuna c, List<Servicio> s) {
		super("icono CGP", direccion);
		
		if(c == null || s == null)
			throw new NullParametro("Ingrese valores no nulos");
		
		this.comuna = c;
		this.setServicios(s);
	}

	public Comuna getComuna() {
		return comuna;
	}

	public List<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(List<Servicio> servicios) {
		this.servicios = servicios;
	}

	@Override
	public boolean esCercano(Point unPunto) {
		return super.esCercano(unPunto) || this.comuna.esCercano(unPunto);
	}

	@Override
	public boolean estaDisponible(Instant instante) {
		return this.servicios.stream().anyMatch(
				servicio -> servicio.estaDisponible(instante));
	}

	@Override
	public boolean estaDisponible(Instant instante, String servicio) {
		Servicio servicioSel = this.getServicio(servicio);

		if (servicioSel == null)
			return false;
		else
			return servicioSel.estaDisponible(instante);
	}

	public Servicio getServicio(String nombre) {
		Servicio sel = null;

		for (Servicio servicio : this.servicios) {
			if (servicio.getNombre() == nombre)
				sel = servicio;
		}

		return sel;
	}

	@Override
	public boolean estaEnTexto(String texto) {
		return (comuna.getNumero().equals(texto) || this
				.contieneServicio(texto));
	}

	public boolean contieneServicio(String texto) {
		return this.getServicios().stream()
				.anyMatch(servicio -> servicio.getNombre().startsWith(texto));
	}

}