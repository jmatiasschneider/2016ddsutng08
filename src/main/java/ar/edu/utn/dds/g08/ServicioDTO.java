package ar.edu.utn.dds.g08;

import java.util.List;

public class ServicioDTO {
private String nombre;
private List<rangoServicioDTO> rangos;
public List<rangoServicioDTO> getRangos() {
	return rangos;
}
public void setRangos(List<rangoServicioDTO> rangos) {
	this.rangos = rangos;
}
public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
}
