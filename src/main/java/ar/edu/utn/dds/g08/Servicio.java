package ar.edu.utn.dds.g08;

import org.joda.time.Instant;

import java.util.List;

public class Servicio {
	protected String nombre;
	protected List<Horario> horarios;

	public Servicio(String nombre, List<Horario> horarios) {
		if(nombre == null || horarios == null)
			throw new NullParametro("Ingrese valores no nulos");
		
		this.nombre = nombre;
		this.setHorarios(horarios);
	}

	public String getNombre() {
		return nombre;
	}

	public List<Horario> getHorarios() {
		return this.horarios;
	}

	public void setHorarios(List<Horario> horarios) {
		this.horarios = horarios;
	}

	public boolean estaDisponible(Instant instante){
		return this.horarios.stream().anyMatch(horario -> horario.estaDisponible(instante));
	}
}
