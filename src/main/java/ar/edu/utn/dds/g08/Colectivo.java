package ar.edu.utn.dds.g08;

import java.util.List;

import org.uqbar.geodds.Point;

public class Colectivo extends PuntoDeInteres {

	protected List<Parada> paradas; // tiene q estar ordenada, para saber el recorrido
	protected String linea;

	public Colectivo(String icono, Direccion direccion, String linea,
			List<Parada> paradas) {
		super(icono, direccion); // la dirección es la terminal
		
		if(linea == null || paradas == null)
			throw new NullParametro("Ingrese valores no nulos");
		
		this.linea = linea;
		this.setParadas(paradas);
	}

	public List<Parada> getParadas() {
		return this.paradas;
	}

	public void setParadas(List<Parada> paradas) {
		this.paradas = paradas;
	}

	@Override
	public boolean esCercano(Point unPunto) {
		return this.paradas.stream().anyMatch(
				parada -> parada.esCercano(unPunto));
	}

	public String getLinea() {
		return this.linea;
	}

	@Override
	public boolean estaEnTexto(String texto) {
		return this.linea.equals(texto);
	}

}
