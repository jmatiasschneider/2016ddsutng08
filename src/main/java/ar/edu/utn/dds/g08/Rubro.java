package ar.edu.utn.dds.g08;

public class Rubro {
	private String nombre;
	private double radio;

	protected Rubro() {

	}

	public Rubro(String nombre, double radio) {
		this();

		if (nombre == null)
			throw new NullParametro("Ingrese valores no nulos");

		this.setRadio(radio);
		this.nombre = nombre;
	}

	public double getRadio() {
		return radio;
	}

	public void setRadio(double radio) {
		this.radio = radio;
	}

	public String getNombre() {
		return this.nombre;
	}

}
