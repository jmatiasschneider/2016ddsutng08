package ar.edu.utn.dds.g08;

import java.util.List;

import org.uqbar.geodds.*;
import org.joda.time.*;

public class LocalComercial extends PuntoDeInteres {

	protected Rubro rubro;
	protected List<String> palabrasClaves;
	protected List<Horario> horarios;
	protected String nombreFantasia;

	public LocalComercial(String icono, Direccion direccion, Rubro r,
			List<String> pc, List<Horario> h, String n) {
		super(icono, direccion);

		if(r == null || pc == null || h == null || n == null)
			throw new NullParametro("Ingrese valores no nulos");
		
		this.setRubro(r);
		this.setPalabrasClaves(pc);
		this.setHorarios(h);
		this.setNombre(n);
	}

	public Rubro getRubro() {
		return rubro;
	}

	public void setRubro(Rubro rubro) {
		this.rubro = rubro;
	}

	public void setNombre(String nombre) {
		this.nombreFantasia = nombre;
	}

	public List<String> getPalabrasClaves() {
		return palabrasClaves;
	}

	public void setPalabrasClaves(List<String> palabrasClave) {
		this.palabrasClaves = palabrasClave;
	}

	public void addPalabraClave(String palabraClave) {
		this.palabrasClaves.add(palabraClave);
	}

	public List<Horario> getHorarios() {
		return horarios;
	}

	public void setHorarios(List<Horario> horarios) {
		this.horarios = horarios;
	}

	@Override
	public boolean esCercano(Point unPunto) {
		return this.direccion.distancia(unPunto) < this.rubro.getRadio();
	}

	@Override
	public boolean estaDisponible(Instant instante) {
		return this.horarios.stream().anyMatch(
				horario -> horario.estaDisponible(instante));
	}

	@Override
	public boolean estaDisponible(Instant instante, String servicio) {
		return this.estaDisponible(instante);
	}

	@Override
	public boolean estaEnTexto(String texto) {
		return (this.nombreFantasia.startsWith(texto)
				|| this.palabrasClaves.contains(texto) || this.rubro
				.getNombre().equals(texto));
	}
}