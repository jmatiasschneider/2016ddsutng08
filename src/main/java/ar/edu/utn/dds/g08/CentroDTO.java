package ar.edu.utn.dds.g08;

import java.util.List;

public class CentroDTO {
	private int comuna;
	private String zona;
	private String director;
	private String domicilio;
	private String telefono;
	private List<ServicioDTO> listaDTO;
	
	
	public CentroDTO(int c, String z, String dir, String dom, String tel, List<ServicioDTO> l){
		this.setComuna(c);
		this.setZona(z);
		this.setDirector(dir);
		this.setDomicilio(dom);
		this.setTelefono(tel);
		this.setListaDTO(l);
				
	}


	public List<ServicioDTO> getListaDTO() {
		return listaDTO;
	}


	public void setListaDTO(List<ServicioDTO> listaDTO) {
		this.listaDTO = listaDTO;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public String getDomicilio() {
		return domicilio;
	}


	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}


	public String getDirector() {
		return director;
	}


	public void setDirector(String director) {
		this.director = director;
	}


	public String getZona() {
		return zona;
	}


	public void setZona(String zona) {
		this.zona = zona;
	}


	public int getComuna() {
		return comuna;
	}


	public void setComuna(int comuna) {
		this.comuna = comuna;
	}
}
