package ar.edu.utn.dds.g08;

public class rangoServicioDTO {
private int numeroDia;
private int horarioDesde;
private int minutosDesde;
private int horarioHasta;
private int minutosHasta;
public int getMinutosHasta() {
	return minutosHasta;
}
public void setMinutosHasta(int minutosHasta) {
	this.minutosHasta = minutosHasta;
}
public int getHorarioHasta() {
	return horarioHasta;
}
public void setHorarioHasta(int horarioHasta) {
	this.horarioHasta = horarioHasta;
}
public int getMinutosDesde() {
	return minutosDesde;
}
public void setMinutosDesde(int minutosDesde) {
	this.minutosDesde = minutosDesde;
}
public int getHorarioDesde() {
	return horarioDesde;
}
public void setHorarioDesde(int horarioDesde) {
	this.horarioDesde = horarioDesde;
}
public int getNumeroDia() {
	return numeroDia;
}
public void setNumeroDia(int numeroDia) {
	this.numeroDia = numeroDia;
}
}
