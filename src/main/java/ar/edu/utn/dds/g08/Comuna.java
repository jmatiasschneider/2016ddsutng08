package ar.edu.utn.dds.g08;

import org.uqbar.geodds.*;

public class Comuna {
	protected String numero;
	protected Polygon radio;

	protected Comuna() {

	}
	
	public Comuna(String n, Polygon r) {
		this();
		
		if(n == null || r == null)
			throw new NullParametro("Ingrese valores no nulos");
		
		this.setNumero(n);
		this.setRadio(r);

	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNumero() {
		return numero;
	}

	public void setRadio(Polygon radio) {
		this.radio = radio;
	}

	public void agregarPunto(Point unPunto) {
		this.radio.add(unPunto);
	}

	public boolean esCercano(Point unPunto) {
		return this.radio.isInside(unPunto);
	}

}
