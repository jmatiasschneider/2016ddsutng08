package ar.edu.utn.dds.g08;

import org.uqbar.geodds.*;

public class Direccion {

	protected String pais;
	protected String provincia;
	protected String localidad;
	protected String barrio;
	protected String calle;
	protected int numero;
	protected int piso;
	protected String depto;
	protected int unidad;
	protected String cp;
	protected Point ubicacion;

	protected Direccion() {

	}

	public Direccion(Point ubicacion) {
		this();

		if (ubicacion == null)
			throw new NullParametro("Ingrese valores no nulos");
		
		this.ubicacion = ubicacion;
	}

	public Direccion(String pais, String provincia, String localidad,
			String barrio, String calle, int numero, int piso, String depto,
			int unidad, String cp, Point ubicacion) {
		this();

		if (ubicacion == null)
			throw new NullParametro("Ingrese valores no nulos");

		this.pais = pais;
		this.provincia = provincia;
		this.localidad = localidad;
		this.barrio = barrio;
		this.calle = calle;
		this.numero = numero;
		this.piso = piso;
		this.depto = depto;
		this.unidad = unidad;
		this.cp = cp;
		this.ubicacion = ubicacion;

	}

	public boolean esCercano(Point ubicacion) {
		return (this.ubicacion.distance(ubicacion) < 0.5);
	}

	public double distancia(Point ubicacion) {
		return this.ubicacion.distance(ubicacion);
	}
}
