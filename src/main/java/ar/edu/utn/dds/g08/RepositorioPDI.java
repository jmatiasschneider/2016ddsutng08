package ar.edu.utn.dds.g08;

import java.util.*;
import java.util.stream.Collectors;

public class RepositorioPDI {

	private HashMap<Integer, PuntoDeInteres> collectionPDI = new HashMap<Integer, PuntoDeInteres>();
	private static RepositorioPDI instance = null;
	private int key;

	protected RepositorioPDI() {
	}

	public RepositorioPDI getInstance() {
		if (instance == null) {
			this.setKey(0);
			return instance = new RepositorioPDI();
		} else
			return instance;
	}

	public static void clear() {
		instance = new RepositorioPDI();
	}

	public void addPDI(PuntoDeInteres newPDI) {
	
		if(!collectionPDI.containsValue(newPDI))
		{
			collectionPDI.put(this.key, newPDI);
			this.setKey(this.key+1);
		}
		else
			System.out.println("Objeto ya existe");
	}

	
	
	public void removePDI(Integer key) {
		try{
		collectionPDI.remove(key);
		}
		catch (NullPointerException e) {
			System.out.println("NO existe objeto con esa key");
		}
	}

	public void modifyPDI(Integer key, PuntoDeInteres PDI) {
		collectionPDI.replace(key, PDI);
	}

	
	
	public List<PuntoDeInteres> findPDIbyKey(Integer key) {
		List<PuntoDeInteres> listaPDI = new ArrayList<PuntoDeInteres>();
		listaPDI.add(collectionPDI.get(key));
		return listaPDI;
	}
	
	public List<PuntoDeInteres> findPDIbyString(String texto) {
		List<PuntoDeInteres> listaPDI = collectionPDI.entrySet()
				.stream()
				.filter(e -> e.getValue().estaEnTexto(texto))
				.map(Map.Entry::getValue)
				.collect(Collectors.toList());
		return listaPDI;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}
}
