package ar.edu.utn.dds.g08;

import java.util.List;

import org.uqbar.geodds.*;

public class Parada extends PuntoDeInteres {
	protected List<Colectivo> lineas;

	public Parada(String icono, Direccion direccion) {
		super(icono, direccion);
		
		// las lineas en principio van vacías
	}

	public void setLineas(List<Colectivo> lineas) {
		this.lineas = lineas;
	}

	public List<Colectivo> getLineas(){
		return this.lineas;
	}
	
	@Override
	public boolean esCercano(Point unPunto) {
		return this.direccion.distancia(unPunto) < 0.1;
	}

	@Override
	public boolean estaEnTexto(String texto) {
		return this.lineas.stream().anyMatch(linea -> linea.estaEnTexto(texto));
	}
}
