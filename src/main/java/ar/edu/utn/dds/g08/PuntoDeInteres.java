package ar.edu.utn.dds.g08;

import org.uqbar.geodds.*;
import org.joda.time.*;

public abstract class PuntoDeInteres {

	protected String icono; // falta revisar de qué tipo será
	protected Direccion direccion; // Incluye la ubicación

	protected PuntoDeInteres() {

	}

	public PuntoDeInteres(String icono, Direccion direccion) {
		this();
		
		if(direccion == null || icono == null)
				throw new NullParametro("Ingrese valores no nulos");
		
		this.icono = icono;
		this.setDireccion(direccion);
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public Direccion getDireccion() {
		return this.direccion;
	}

	public boolean esCercano(Point ubicacion) {
		return this.direccion.esCercano(ubicacion);
	}

	public boolean estaDisponible(Instant instante) {
		return true; // ante la duda siempre responde true
	}

	public boolean estaDisponible(Instant instante, String servicio) {
		return true; // ante la duda siempre responde true
	}

	public boolean estaEnTexto(String texto) {
		return true; // ante la duda siempre responde true
	}

	/*
	 * public boolean estaEnTexto(String nombre, String servicio) { // TODO
	 * Auto-generated method stub return true; }
	 */
}
