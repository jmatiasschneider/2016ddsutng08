package ar.edu.utn.dds.g08;

import java.util.List;
import java.util.Arrays;
import org.joda.time.Instant;

public class SucursalBanco extends PuntoDeInteres {

	protected List<Servicio> servicios;
	protected String banco; // va a ser una clase, mínimo con el icono y el horario bancario
	
	public SucursalBanco(Direccion direccion, List<Servicio> servicios, String banco) {
		super("banco.icono",direccion);

		if(banco == null || servicios == null)
			throw new NullParametro("Ingrese valores no nulos");

		this.servicios = servicios;
		this.banco = banco;
		
		List<Horario> horarios = Arrays.asList(new Horario(1, 5, 10, 00, 15, 00 ));

		this.servicios.stream().forEach(servicio -> servicio.setHorarios(horarios));
	}

	public List<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(List<Servicio> servicios) {
		this.servicios = servicios;
	}

	public boolean estaDisponible(Instant instante) {
		// se sabe que todos los servicios tienen el mismo horario
		return this.servicios.stream().anyMatch(servicio -> servicio.estaDisponible(instante));
	}

	@Override
	public boolean estaDisponible(Instant instante, String servicio) {
		Servicio servicioSel = this.getServicio(servicio);

		if (servicioSel == null)
			return false;
		else
			return servicioSel.estaDisponible(instante);
	}

	public Servicio getServicio(String nombre) {
		Servicio sel = null;

		for (Servicio servicio : this.servicios) {
			if (servicio.getNombre() == nombre)
				sel = servicio;
		}

		return sel;
	}

	@Override
	public boolean estaEnTexto(String texto) {
		return this.banco.startsWith(texto);
	}
}